
import { strings } from '../res/Strings'

export const getData = () => {
    return (dispatch) => {


        fetch('https://api.jsonbin.io/b/5f2c36626f8e4e3faf2cb42e')
            .then(res => { return res.json() })
            .then(jsonResponse => {
                console.log("DATA:= " + JSON.stringify(jsonResponse))
                dispatch({ type: strings.FETCH_DATA_SUCCESS, payload: jsonResponse.categories })
            }).catch((err) => {
                dispatch({ type: strings.FETCH_DATA_ERROR, payload: strings.NETWORK_ERROR })
            })
    }
}
