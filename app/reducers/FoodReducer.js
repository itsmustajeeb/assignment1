
import { strings } from "../res/Strings";

const FoodReducer = (state = '', action) => {
    switch (action.type) {

        case strings.FETCH_DATA_SUCCESS: {
            return action.payload
        }

        case strings.FETCH_DATA_ERROR: {
            return action.payload
        }
    }
    return state
}
export default FoodReducer;