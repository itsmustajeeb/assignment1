import React, { Component } from 'react'
import { View, Image, Text, TouchableOpacity, FlatList, SafeAreaView } from 'react-native'
import { image } from '../res/images/images'
import { styles } from '../utils/StyleSheet'


export default class MenuItems extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isSelected: false,
        }
    }

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 0.5,
                    width: "100%",
                    backgroundColor: "lightgray",
                }}
            />
        );
    };

    setImg(isSelected) {
        if (isSelected) {
            return image.expand
        } else {
            return image.collpse
        }
    }

    checkUndefined(data) {
        if (data == '' || data == 'undefined') {
            return false
        } else return true
    }




    renderDetails = (subcategoryData, category) => (
        <SafeAreaView >
            <View >
                <View style={[styles.roundedWhite, { marginTop: 0, elevation: 1, width: '100%', flexDirection: 'column', flex: 1 }]}>
                    <FlatList
                        horizontal={false}
                        data={subcategoryData}
                        renderItem={(({ item }) =>
                            <View >
                                <Text style={{ color: category.colorCode, marginTop: 5, fontSize: 18, marginStart: 5 }}>{item.subCategoryname}</Text>
                                <FlatList
                                    data={item.items}
                                    renderItem={(({ item }) =>
                                        <View style={{ backgroundColor: 'white', padding: 8 }}>
                                            <Text >{item}</Text>
                                        </View>
                                    )}
                                    ItemSeparatorComponent={this.renderSeparator}
                                    keyExtractor={item => item.id}
                                />

                                {this.checkUndefined(category.quote) ? <View style={styles.quotesText}>
                                    <Text >{category.quote}</Text>
                                </View> : null}

                            </View>

                        )}
                    />
                </View>

                {this.checkUndefined(category.protip) ?
                    <View style={{ alignSelf: 'center', minHeight: 90, backgroundColor: 'blue', width: '100%', margin: 15, borderRadius: 10, elevation: 5 }}>
                        <View style={{ borderRadius: 10, alignSelf: 'flex-start', marginStart: 10, marginTop: 15, backgroundColor: 'rgba(224, 224, 235, 0.3)', paddingStart: 10, paddingEnd: 10 }}>
                            <Text style={{ color: 'white' }}>Tip</Text>
                        </View>
                        <Text style={{ color: 'white', margin: 10 }} >{category.protip}</Text>
                    </View>
                    : null}


            </View>

        </SafeAreaView>
    )

    render() {
        return (
            <SafeAreaView >

                <View >
                    <TouchableOpacity
                        style={[styles.roundedWhiteBackground, { flexDirection: 'row' }]}
                        onPress={() => {
                            this.setState({
                                isSelected: ((this.state.isSelected) ? false : true)
                            })
                            this.props.action(false)

                        }}>
                        <View style={styles.itemHeaderBackground}>
                            <Image source={image.search}
                                style={{ height: 25, tintColor: this.props.item.category.colorCode, alignSelf: 'center', width: 25 }} />
                        </View>

                        <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                            <Text style={{ marginStart: 10, fontSize: 14, color: this.props.item.category.colorCode }}>{this.props.item.category.categoryName}</Text>
                            {this.checkUndefined(this.props.item.category) ? <Text style={{ marginStart: 10, fontSize: 14 }}>{'(' + this.props.item.category.servingSize + ')'}</Text> : null}
                        </View>

                        <Image source={(this.props.isCollapsed) ? image.expand : this.setImg(this.state.isSelected)}
                            style={{ position: 'absolute', end: 0, marginEnd: 5, height: 8, alignSelf: 'center', width: 8 }} />

                    </TouchableOpacity>

                    {!this.props.isCollapsed && this.state.isSelected && this.renderDetails(this.props.item.category.subcategories, this.props.item.category)}

                </View>

            </SafeAreaView>
        )
    }
}