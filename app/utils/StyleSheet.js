import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    parentView: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#E9E8EE',
        padding: 10
    },
    launchBtnStyle: {
        height: 60, paddingStart: 25, paddingEnd: 25, margin: 10, borderRadius: 1000, alignSelf: 'center',
        backgroundColor: 'green',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        justifyContent: 'center',
    },
    roundedWhite: { borderBottomEndRadius: 10, borderBottomStartRadius: 10, shadowOffset: { width: 0, height: 5 }, shadowOpacity: 0.5, elevation: 2, shadowRadius: 5, backgroundColor: 'white', padding: 5 },
    roundedWhiteBackground: { borderRadius: 5, shadowOffset: { width: 0, height: 5 }, shadowOpacity: 0.5, elevation: 2, shadowRadius: 5, marginTop: 15, backgroundColor: 'white', padding: 5 },
    btnTxt: { fontSize: 18, alignSelf: 'center', color: 'white', fontWeight: 'bold' },
    itemSubHeaderBackground: { backgroundColor: 'white', justifyContent: 'center', padding: 5, width: '100%' },
    quotesText: { borderRadius: 10, marginTop: 15, alignSelf: 'center', backgroundColor: 'rgba(224, 224, 235, 0.9)', padding: 8 },
    itemHeaderBackground: { height: 40, width: 40, justifyContent: 'center', backgroundColor: 'rgba(224, 224, 235, 0.9)' }
});

export { styles }
