import React, { Component } from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import LaunchButton from '../launchButton/LaunchButton'
import FoodList from '../foodList/foodList'


const mainNavigator = createStackNavigator(
    {
        launchBtn: { screen: LaunchButton },
        foodList: { screen: FoodList }

    },
    { initialRouteName: 'launchBtn' },
);

const app = createAppContainer(mainNavigator);
export default app;
