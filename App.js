import React, { Component } from 'react';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import Navigator from './app/navigation/AppNavigation'
import { NavigationContainer } from '@react-navigation/native';
import FoodReducer from './app/reducers/FoodReducer'

// combine all reduces
const masterReducer = combineReducers({
  foodData: FoodReducer
});

//create store with reduces
const store = createStore(
  masterReducer,
  {
    foodData: [],

  },
  applyMiddleware(thunk),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());


export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      backPressed: 0,
    };
  }



  render() {
    return (
      <Provider store={store}>
        <NavigationContainer>
          <Navigator />
        </NavigationContainer>
      </Provider>
    );
  }
}
