import React, { Component } from 'react';
import { connect } from 'react-redux'
import { styles } from '../utils/StyleSheet'
import { strings } from '../res/Strings'
import { image } from '../res/images/images'
import MenuItem from './MenuItem'
import { SafeAreaView, TouchableOpacity, Image, Text, View, FlatList } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';

class FoodList extends Component {
    static navigationOptions = {
        header: null
    }
    constructor(props) {
        super(props);
        this.state = {
            SearchInputText: '',
            isCollapsed: false,
        };
    }

    handler = (v) => {
        this.setState({
            isCollapsed: v
        });
    }

    setDataInList = (label) => {
        if (label == '') {
            return this.props.foodData
        }
        else {
            const newData = this.props.foodData.filter((item) => { return item.category.categoryName == label })
            return newData
        }
    }



    render() {

        return (
            <SafeAreaView
                style={styles.parentView}
            >
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ flex: 1 }}>

                        <TouchableOpacity
                            onPress={() => { this.props.navigation.navigate('launchBtn') }}
                            style={{
                                height: 30, margin: 5, width: 30, justifyContent: 'center'
                            }}>
                            <Image
                                style={{ height: 20, tintColor: 'black', alignSelf: 'center', width: 20 }}
                                source={image.close} />
                        </TouchableOpacity>

                        <Text style={[styles.btnTxt, { marginTop: 15, alignSelf: 'flex-start', fontSize: 25, color: 'black' }]}>{strings.approvedFoodList}</Text>

                        <View
                            style={{ borderRadius: 5, flexDirection: 'row', justifyContent: 'flex-start', marginTop: 10, backgroundColor: 'rgba(224, 224, 235, 0.9)' }}
                        >
                            <Image

                                style={{ height: 15, marginStart: 15, tintColor: 'gray', alignSelf: 'center', width: 15 }}
                                source={image.search} />

                            <TextInput
                                returnKeyType={'done'}
                                numberOfLines={1}
                                style={{ color: 'black', fontSize: 12, marginStart: 5, }}
                                placeholder="Try searching fat, sauces names..."
                                onChangeText={SearchInputText => this.setState({ SearchInputText })} />

                        </View>

                        <FlatList
                            data={this.setDataInList(this.state.SearchInputText)}
                            renderItem={({ item }) => (
                                <MenuItem
                                    item={item}
                                    isCollapsed={this.state.isCollapsed}
                                    action={this.handler}
                                />)}
                        />

                    </View>
                </ScrollView>
            </SafeAreaView >
        );
    }
}



const mapStateToProps = state => {
    return {
        foodData: state.foodData
    };
};

const dispatchStateToProps = dispatch => {
    return {

    };
};


export default connect(mapStateToProps, dispatchStateToProps)(FoodList);
