import React, { Component } from 'react';
import { connect } from 'react-redux'
import { styles } from '../utils/StyleSheet'
import { strings } from '../res/Strings'
import { getData } from '../actions/ListAction'
import { SafeAreaView, TouchableOpacity, Text, View } from 'react-native';

class LaunchButton extends Component {
    static navigationOptions = {
        header: null
    }


    render() {

        return (
            <SafeAreaView
                style={styles.parentView}
            >
                <TouchableOpacity
                    onPress={() => {
                        this.props.hitFoodDataAPI()
                        this.props.navigation.navigate('foodList')
                    }}
                    style={styles.launchBtnStyle}>
                    <Text style={styles.btnTxt}>{strings.approvedFoodList}</Text>
                </TouchableOpacity>



            </SafeAreaView >
        );
    }
}



const mapStateToProps = state => {
    return {

    };
};

const dispatchStateToProps = dispatch => {
    return {
        hitFoodDataAPI: () => { dispatch(getData()) }
    };
};


export default connect(mapStateToProps, dispatchStateToProps)(LaunchButton);
