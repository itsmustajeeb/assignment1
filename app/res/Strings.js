const strings = {
    approvedFoodList: 'Approved Food List',
    FETCH_DATA_SUCCESS: 'FETCH_DATA_SUCCESS',
    FETCH_DATA_ERROR: 'FETCH_DATA_ERROR'

};
export { strings };